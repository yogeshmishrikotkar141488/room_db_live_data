package com.yogi.roomlivedata;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.yogi.roomlivedata.database.RepoDatabase;
import com.yogi.roomlivedata.entity.Repo;

public class MainActivity extends AppCompatActivity {

    private TextView displayText;

    int id = 2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        findViewById(R.id.displayButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        final Repo repo = RepoDatabase
                                .getInstance(getApplicationContext())
                                .getRepoDao().getRepo(id);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                displayText.setText(repo.name);
                            }
                        });

                    }
                }).start();

            }
        });

        findViewById(R.id.saveButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        id++;
                        RepoDatabase
                                .getInstance(getApplicationContext())
                                .getRepoDao()
                                .insert(new Repo(id, "Cool Repo Name"+id, "url"));
                    }
                }).start();

            }
        });

         displayText= findViewById(R.id.displayText);

    }
}
