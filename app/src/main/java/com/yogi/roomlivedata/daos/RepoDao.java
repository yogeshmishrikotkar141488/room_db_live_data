package com.yogi.roomlivedata.daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.database.Cursor;

import com.yogi.roomlivedata.entity.Repo;

import java.util.List;

@Dao
public interface RepoDao {

    @Query("SELECT * FROM repo")
    List<Repo> getAllRepos();

    @Insert
    void insert(Repo repos);

    @Update
    void update(Repo  repos);

    @Delete
    void delete(Repo  repos);

    @Query("SELECT * FROM repo WHERE id=:id")
    Repo getRepo(int id);

    @Query("SELECT * FROM repo")
    Cursor getRepoCursor();


    @Query("SELECT * FROM repo WHERE name=:name")
    List<Repo> getReposByName(String name);

    @Query("SELECT * FROM repo WHERE name=:name LIMIT :max")
    List<Repo> getReposByName(int max, String... name);

}
